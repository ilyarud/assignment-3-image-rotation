//
// Created by ilya on 26.11.23.
//

#include <stdint.h>

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H


struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

// allocate memory for new image
struct image image_new(uint64_t width, uint64_t height);

// free allocated memory
void image_destroy(struct image* img);

#endif //IMAGE_TRANSFORMER_IMAGE_H
