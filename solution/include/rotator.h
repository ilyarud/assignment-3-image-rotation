//
// Created by ilya on 26.11.23.
//

#include "image.h"
#include "status.h"

#ifndef IMAGE_TRANSFORMER_ROTATOR_H
#define IMAGE_TRANSFORMER_ROTATOR_H

enum rotation_status rotate(struct image* img, struct image* rot,int16_t angle);

#endif //IMAGE_TRANSFORMER_ROTATOR_H
