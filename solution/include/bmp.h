//
// Created by ilya on 26.11.23.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "stdint.h"
#include "stdio.h"


#include "image.h"
#include "status.h"

struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum file_read_status from_bmp(FILE* in, struct image* img);
enum file_write_status to_bmp(FILE* out, struct image const* img);

#endif //IMAGE_TRANSFORMER_BMP_H
