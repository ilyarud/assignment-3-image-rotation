//
// Created by ilya on 26.11.23.
//

#include "image.h"
#include "status.h"

#include "stdint.h"
#include "stdio.h"


#ifndef IMAGE_TRANSFORMER_VALIDATOR_H
#define IMAGE_TRANSFORMER_VALIDATOR_H

int validate_angle(int16_t angle);

void print_err(const char* message);

enum file_status open_file(const char* path, FILE** file, const char* mode);
enum file_status close_file(FILE* file);


#endif //IMAGE_TRANSFORMER_VALIDATOR_H

