//
// Created by ilya on 26.11.23.
//

#include "stdio.h"

#ifndef IMAGE_TRANSFORMER_STATUS_H
#define IMAGE_TRANSFORMER_STATUS_H

enum file_read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BIT_COUNT,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
};

enum file_write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum file_status {
    FILE_OK = 0,
    CANT_OPEN_FILE,
    FILE_DOESNT_EXIST,
    CANT_CLOSE_FILE
};

enum rotation_status {
    ROTATION_OK = 0,
    ROTATION_ERROR
};

#endif //IMAGE_TRANSFORMER_STATUS_H
