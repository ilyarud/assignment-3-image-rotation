#include <stdlib.h>

#include "bmp.h"
#include "rotator.h"
#include "utils.h"

#define READ_MODE "rb"
#define WRITE_MODE "wb"

int main( int argc, char* argv[] ) {

    if (argc != 4) {
        print_err("USAGE:\n./image-transformer <source-image> <transformed-image> <angle>");
    }

    const int16_t angle = (int16_t) strtol(argv[3], NULL, 10);
    if (!validate_angle(angle))
        print_err("Wrong angle passed.\n");

    // open src file
    FILE* src_file = { 0 };
    enum file_status open_src_status = open_file(argv[1], &src_file, READ_MODE);
    if (open_src_status != 0)
        print_err("Error occurred while opening src file\n");

    // read src from file
    struct image src_img = { 0 };
    enum file_read_status read_status = from_bmp(src_file, &src_img);
    printf("read_status: %d\n", read_status);
    if (read_status != 0)
        print_err("Error occurred while parsing file");

    // close src file
    enum file_status src_close_status = close_file(src_file);
    printf("src_close_status: %d\n", src_close_status);
    if (src_close_status != 0)
        print_err("Error occurred while closing source file\n");

    // create (allocate memory for) rotated image struct
    struct image rotated = image_new(src_img.width, src_img.height);

    // rotate src image and write result to 'rotated'
    enum rotation_status rot_status = rotate(&src_img, &rotated, angle);
    if (rot_status != 0)
        print_err("Error occurred while rotating image");

  // open (create) dest. file
    FILE* dest_file = { 0 };
    enum file_status open_dest_status = open_file(argv[2], &dest_file, WRITE_MODE);
    if (open_dest_status != 0)
        print_err("Error occurred while opening dest. file\n");

    // write to dest. file
    enum file_write_status write_status = to_bmp(dest_file, &rotated);
    if (write_status != 0)
        print_err("Error occurred while saving image to file");

    // close dest. file
    enum file_status dest_close_status = close_file(dest_file);
    if (dest_close_status != 0)
        print_err("Error occurred while closing dest. file\n");

    // free other resources
    image_destroy(&rotated);
    if (angle != 0)
      image_destroy(&src_img);


    fprintf(stdin, "Image %s rotated successfully by %d degrees and saved to %s\n", argv[1], angle, argv[2]);

    return 0;
}
