//
// Created by ilya on 1.12.23.
//


#include "status.h"
#include "bmp.h"
#include "image.h"

#define H_TYPE 0x4D42
#define BIT_COUNT 24

static uint32_t row_size(uint64_t w) {
    // https://en.wikipedia.org/wiki/BMP_file_format#Pixel_storage
    uint32_t size = (sizeof(struct pixel) * 8 * w + 31) / 32 * 4;

    return size;
}


struct bmp_header create_header(const struct image* img) {

    const uint32_t image_size = row_size(img->width) * img->height;

    struct bmp_header h = {
        .bfType = H_TYPE,
        .bfileSize = sizeof(struct bmp_header) + image_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = image_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };

    return h;
}


enum file_read_status from_bmp(FILE* in, struct image* img) {

    struct bmp_header h;

    if (fread(&h, sizeof(struct bmp_header), 1, in) != 1) {
        return READ_INVALID_HEADER;
    }
    if (h.bfType != H_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if (h.biBitCount != BIT_COUNT) {
        return READ_INVALID_BIT_COUNT;
    }

    *img = image_new(h.biWidth, h.biHeight);
    if (!(img->data)) {
      return READ_INVALID_BITS;
    }

    const uint8_t padding = row_size(h.biWidth) - sizeof(struct pixel) * h.biWidth;
    for (uint32_t i = 0; i < h.biHeight; i++) {
        fread(&img->data[i * h.biWidth], sizeof(struct pixel), h.biWidth, in);
        fseek(in, padding, SEEK_CUR);
    }

    return READ_OK;
}

enum file_write_status to_bmp(FILE* out, struct image const* img) {

    struct bmp_header h = create_header(img);

    if (fwrite(&h, sizeof(struct bmp_header), 1, out) != 1)
        return WRITE_ERROR;

    const uint8_t padding = row_size(img->width) - sizeof(struct pixel) * img->width;

    for (uint32_t i = 0; i < h.biHeight; i++) {
      fwrite((img->data + i * h.biWidth), sizeof(struct pixel), h.biWidth, out);
      fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}

