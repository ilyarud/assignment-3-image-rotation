//
// Created by ilya on 26.11.23.
//

#include "image.h"

#include "stdlib.h"

struct pixel* get_pixels(uint64_t w, uint64_t h) {
    struct pixel* size = malloc(sizeof(struct pixel) * w * h);
    return size;
}

struct image image_new(uint64_t width, uint64_t height) {

    struct image img = {
        .width = width,
        .height = height,
        .data = get_pixels(width, height)
    };

    return img;
}

void image_destroy(struct image* img) {
    if (img->data) {
        free(img->data);
    }
}
