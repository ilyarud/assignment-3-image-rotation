//
// Created by ilya on 2.12.23.
//

#include "image.h"
#include "rotator.h"
#include "status.h"

#include "string.h"

// dim = dimension
static void change_dim(struct image *img, struct image *new) {
  new->width = img->height;
  new->height = img->width;
}

static void copy_image(struct image *img, struct image *new) {
  memcpy(new, img, sizeof(*img));
}

static void rotate_90(struct image *img, struct image *new) {

  change_dim(img, new);

  for (uint32_t i = 0; i < new->height; i++) {
    for (uint32_t j = 0; j < new->width; j++) {
      new->data[new->width * i + j] = img->data[img->width * j + (img->width - i - 1)];
    }
  }
}

static void rotate_180(struct image *img, struct image *new) {

  for (uint64_t i = 0; i < img->height; i++) {
    for (uint64_t j = 0; j < img->width; j++) {
      new->data[(new->height - i - 1) * new->width + (new->width - j - 1)] = img->data[img->width * i + j];
    }
  }
}

static void rotate_270(struct image *img, struct image *new) {

  struct image temp = image_new(img->width, img->height);

  rotate_180(img, &temp);
  rotate_90(&temp, new);

  image_destroy(&temp);
}

// img - src;
// rot - rotated
enum rotation_status rotate(struct image *img, struct image *rot, int16_t angle) {

  switch (angle) {
    case 0:
      image_destroy(rot);
      copy_image(img, rot);
      break;
    case 90:
    case -270:
      rotate_90(img, rot);
      break;
    case 180:
    case -180:
      rotate_180(img, rot);
      break;
    case 270:
    case -90:
      rotate_270(img, rot);
      break;
    default:
      return ROTATION_ERROR;
  }

  return ROTATION_OK;
}

