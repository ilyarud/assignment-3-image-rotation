//
// Created by ilya on 26.11.23.
//

#include "utils.h"

#include "errno.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

int validate_angle(int16_t angle) {
    const int angles[] = {0, 90, -90, 180, -180, 270, -270};
    const size_t array_size = sizeof(angles) / sizeof(angles[0]);

    for (int i =0; i < array_size; i++) {
        if (angle == angles[i]) {
            return 1;
        }
    }
    return 0;
}

void print_err(const char* message) {
    fprintf(stderr, "%s\n", message);
    exit(1);
}


enum file_status open_file(const char* path, FILE** file, const char* mode) {
    *file = fopen(path, mode);
    if (!file) {
        return CANT_OPEN_FILE;
    }
    return FILE_OK;
}



enum file_status close_file(FILE* file) {
    if (!file)
        return FILE_DOESNT_EXIST;
    if (fclose(file) != 0)
        return CANT_CLOSE_FILE;

    return FILE_OK;
}
